import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        numbers.add(12);
        numbers.add(3);
        numbers.add(48);
        numbers.add(7);
        numbers.add(26);

        int max = Collections.max(numbers);
        int min = Collections.min(numbers);

        System.out.println("Maximum value: " + max);
        System.out.println("Minimum value: " + min);
    }
}